# BM Wordpress Docker

### Usage Guide
1. Install docker & docker-compose
2. add local-jobs.bringmeister.de to your /etc/hosts:
    127.0.0.1 local-jobs.bringmeister.de
3. CD into/your/documentRoot 
4. $ docker-compose build && docker-compose up
5. in your browser, go to: http://local-jobs.bringmeister.de
6. Admin Login: /wp-admin => admin:test123
