<?php
add_filter( 'mce_buttons_2', 'xcore_bm_mce_editor_buttons' );
function xcore_bm_mce_editor_buttons( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}

add_filter( 'tiny_mce_before_init', 'xcore_bm_mce_before_init' );
function xcore_bm_mce_before_init( $settings ) {

    $style_formats = array(
        array(
            'title' => 'Paragraph C1',
            'selector' => 'p',
            'classes' => 'c1',
            'wrapper' => true,
        ),
        array(
            'title' => 'Paragraph C2',
            'selector' => 'p',
            'classes' => 'c2',
            'wrapper' => true,
        ),
        array(
            'title' => 'Paragraph C3',
            'selector' => 'p',
            'classes' => 'c3',
            'wrapper' => true,
        ),
        array(
            'title' => 'Paragraph C4',
            'selector' => 'p',
            'classes' => 'c4',
            'wrapper' => true,
        ),
        array(
            'title' => 'Paragraph C5',
            'selector' => 'p',
            'classes' => 'c5',
            'wrapper' => true,
        ),
        array(
            'title' => 'Paragraph C6',
            'selector' => 'p',
            'classes' => 'c6',
            'wrapper' => true,
        ),
    );

    $settings['style_formats'] = json_encode( $style_formats );

    return $settings;

}