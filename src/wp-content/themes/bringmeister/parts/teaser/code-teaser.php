<?php
/*
 * Vars
 */
global $indicator, $arrows, $interval, $fade, $images;

if($images) { ?>
<section id="teaser" class="<?php echo get_section_layout_class('teaser'); ?>">
	<div id="carousel-teaser" class="carousel slide <?php if('1' == $fade) echo 'carousel-fade'; ?>" data-ride="carousel" data-interval="<?php echo $interval; ?>">
		<?php if("1" != $indicator && count($images) > '1') { ?>
			<ol class="carousel-indicators">
				<?php for($i=0; $i<count($images); $i++) { ?>
					<li data-target="#carousel-teaser" data-slide-to="<?php echo $i; ?>" <?php if($i==0) echo 'class="active"'; ?>></li>
				<?php } ?>
			</ol>
		<?php } ?>
		
		<div class="carousel-inner" role="listbox">
			<?php $i=0; foreach($images as $image) { ?>
				<div class="item<?php if($i==0) echo ' active'; if(!$image['image']) echo ' item-noimg';?>"<?php if($image['background']) echo ' style="background-color:'.$image['background'].'"'; ?>>
					<?php if($image['image']) { ?>
						<?php if(wp_is_mobile() && $image['image_mobile']) { ?>
							<img src="<?php echo $image['image_mobile']['url']; ?>" width="<?php echo $image['image_mobile']['width']; ?>" height="<?php echo $image['image_mobile']['height']; ?>" alt="<?php echo $image['image_mobile']['alt']; ?>" />
						<?php } else { ?>
							<img src="<?php echo $image['image']['url']; ?>" width="<?php echo $image['image']['width']; ?>" height="<?php echo $image['image']['height']; ?>" alt="<?php echo $image['image']['alt']; ?>" />
						<?php } ?>
					<?php } ?>
					<?php if($image['text']) { ?>
						<div class="container">
							<div class="carousel-caption">
								<?php echo $image['text']; ?>
							</div>
						</div>
					<?php } ?>
				</div>
			<?php $i++; } ?>
		</div>
		
		<?php if("1" != $arrows  && count($images) > '1') { ?>
			<a class="left carousel-control" href="#carousel-teaser" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#carousel-teaser" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		<?php } ?>
	</div>
</section>
<?php } ?>
