<?php
/*
 * @TODO: Klassen
 */
?>
<div class="col-sm-4">
	<article <?php post_class('post-grid'); ?>>
		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
			<?php echo xcore_post_thumbnail($post->ID, 'thumbnail', array('class' => 'img-responsive')); ?>
		</a>

		<h2>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<?php the_title(); ?>
			</a>
		</h2>

		<div class="clearfix"></div>
	</article>
</div>