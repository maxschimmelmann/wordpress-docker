<div class="col-sm-6">
    <article <?php post_class(); ?>>
        <h3>
            <a href="<?php the_permalink(); ?>">
                <?php the_title(); ?>
            </a>
        </h3>

        <p class="job-standort"><?php echo strip_tags(get_the_term_list($post->ID, 'job_standort', '', ', ', '')); ?></p>
        <p class="job-bereich pull-right"><span class="badge badge-job"><?php echo strip_tags(get_the_term_list($post->ID, 'job_bereich', '', ', ', '')); ?></span></p>

        <div class="clearfix"></div>
    </article>
</div>