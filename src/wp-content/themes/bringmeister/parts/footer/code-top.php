<div id="footer-top">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-xs-12 col-xxs-12"><?php dynamic_sidebar('footer_1'); ?></div>
            <div class="col-sm-4 col-xs-6 col-xxs-12"><?php dynamic_sidebar('footer_2'); ?></div>
            <div class="col-sm-4 col-xs-6 col-xxs-12"><?php dynamic_sidebar('footer_3'); ?></div>
        </div>
    </div>
</div>