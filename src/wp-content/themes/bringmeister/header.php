<?php do_action('xcore_redirect'); ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><?php wp_title(); ?></title>
		<?php wp_head(); ?>

		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','<?php the_field('gtm_id', 'options'); ?>');</script>
		<!-- End Google Tag Manager -->
	</head>
	
	<body <?php body_class(); ?>>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php the_field('gtm_id', 'options'); ?>" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->

		<!--[if lt IE 8]>
			<p class="browserupgrade"><i class="glyphicon glyphicon-warning-sign"></i> 
				Sie verwenden einen <strong>veralteten</strong> Internet-Browser. Bitte laden Sie sich eine aktuelle Version von <a href="http://browsehappy.com/" target="_blank" rel="nofollow">browsehappy.com</a> um die Seite fehlerfrei zu verwenden.
			</p>
		<![endif]-->
		
		<a href="#content" class="sr-only sr-only-focusable">Skip to main content</a>
		<div id="<?php echo get_wrapper_id(); ?>">
			<?php
			if(xcore_get_topbar())
				get_template_part('parts/topbar/col', '6-6');
			?>

			<header id="header" class="<?php echo get_section_layout_class('header', true); ?>" role="banner">
				<?php
				get_template_part('parts/header/col', xcore_header_structure());
				?>
			</header>

			<?php
			if(xcore_teaser_hide() != "1" && !is_page_template('templates/page-builder.php')) {
				global $indicator, $arrows, $interval, $fade, $images;

				$post_id = 0;

				if(is_tax() || is_category() || is_tag()) {
					$queried_object = get_queried_object();
					$post_id = $queried_object;
				} else if(is_home()) {
					$post_id = get_option('page_for_posts');
				} else {
					if($post)
						$post_id = $post->ID;
				}
				
				$indicator = get_field('teaser_indicator', $post_id);
				$arrows = get_field('teaser_arrows', $post_id);
				$fade = get_field('teaser_fade', $post_id);
				$interval = get_field('teaser_interval', $post_id);
				$images = get_field('teaser_image', $post_id);
				
				get_template_part('parts/teaser/code', 'teaser'); 
			}
			?>
			
			<?php if ( function_exists('yoast_breadcrumb') && ('after_nav' == get_field('design_breadcrumbs_pos', 'option'))) {
				?>
				<section id="breadcrumbs" class="<?php echo get_section_layout_class('breadcrumbs'); ?>">
					<div class="container">
						<?php
						yoast_breadcrumb('<p>','</p>');
						?>
					</div>
				</section>
				<?php
			} ?>