<?php
get_header();

/*
 * VARS
 */
$sidebar = 'left';
?>

<div id="main" class="<?php echo get_section_layout_class('content'); ?>">
    <div class="container">
        <div class="row">
            <?php if($sidebar == 'left') { ?>
                <div class="col-sm-3">
                    <div id="sidebar">
                        <?php get_sidebar(); ?>
                    </div>
                </div>
            <?php } ?>

            <div class="col-sm-<?php if($sidebar == 'none') : echo '12'; else: echo '9'; endif; ?>">
                <div id="content">
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <article <?php post_class(); ?> role="article">
                            <h1><?php the_title(); ?></h1>

                            <div class="job-thumbnail">
                                <?php
                                if(has_post_thumbnail()) {
                                    the_post_thumbnail('full', array('class' => 'img-responsive img-full'));
                                } else {
                                    echo '<img src="' . get_template_directory_uri() . '/_/img/bringmeister-job-placeholder.png" class="img-responsive" />';
                                }
                                ?>
                            </div>

                            <?php the_content(); ?>

                            <div class="clearfix"></div>


                            <div class="job-contact">
                                <p class="h4 gray"><?php _e('Haben wir dein Interesse geweckt?', 'xcore'); ?></p>
                                <p class="h4"><?php _e('Dann sende uns Deine aussagekräftigen Bewerbungsunterlagen zu!', 'xcore'); ?></p>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="inner">
                                            <p class="gray-light"><?php _e('Per Kontaktformular', 'xcore'); ?></p>
                                            <p><a href="<?php echo get_permalink(36538); ?>?job_title=<?php echo get_the_title($post->ID); ?>" class="btn btn-xcore btn-arrow-right btn-lg"><?php _e('Jetzt bewerben!', 'xcore'); ?></a></p>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="inner">
                                            <p class="gray-light"><?php _e('oder auch gerne per E-Mail', 'xcore') ?></p>
                                            <p><a href="mailto:bewerbung@bringmeister.de" class="btn btn-white btn-arrow-right btn-lg">bewerbung@bringmeister.de</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php
                            $args = array(
                                'post_type' => 'job',
                                'posts_per_page' => 2,
                                'post__not_in' => array($post->ID)
                            );

                            $terms = wp_get_post_terms($post->ID, 'job_bereich', array("fields" => "ids"));

                            if($terms) {
                                $args['tax_query'] = array(
                                    array(
                                        'taxonomy' => 'job_bereich',
                                        'fields' => 'ids',
                                        'terms' => $terms
                                    )
                                );
                            }

                            $related = new WP_Query($args);

                            if($related->have_posts()) {
                                ?>
                                <div class="section jobs job-related">
                                    <h2><?php printf(__('%s weitere Jobs im Bereich %s', 'xcore'), ($related->found_posts > 2 ? 2 : $related->found_posts), strip_tags(get_the_term_list($post->ID, 'job_bereich', '', ', ', ''))); ?></h2>
                                    <div class="row">
                                        <?php
                                        while($related->have_posts()) {
                                            $related->the_post();

                                            get_template_part('parts/job/loop', 'grid');
                                        }
                                        ?>
                                    </div>
                                </div>
                                <?php

                                wp_reset_postdata();
                            }
                            ?>
                        </article>
                    <?php endwhile; endif; ?>
                </div>
            </div>

            <?php if($sidebar == 'right') { ?>
                <div class="col-sm-3">
                    <div id="sidebar">
                        <?php get_sidebar(); ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
