<?php 
get_header(); 

/*
 * VARS
 */
$sidebar = xcore_get_sidebar('author');
?>

<div id="main" class="<?php echo get_section_layout_class('content'); ?>">
	<div class="container">
		<div class="row">
			<?php if($sidebar == 'left') { ?>
				<div class="col-sm-4">
					<div id="sidebar">
						<?php get_sidebar(); ?>
					</div>
				</div>
			<?php } ?>
						
			<div class="col-sm-<?php if($sidebar == 'none') : echo '12'; else: echo '8'; endif; ?>">
				<div id="content">
					<h1><?php printf( __('Alle Beiträge von <span>%s</span>', 'xcore'),  get_the_author()); ?></h1>
					
					<?php echo str_replace('avatar-80', 'avatar-80 alignleft',get_avatar( get_the_author_meta('user_email'), $size = '80')); ?>
					<?php if(get_the_author_meta('description') && !is_paged()) { ?>
						<div class="author-description">
							<p class="author-bio">
								<?php the_author_meta( 'description' ); ?>
							</p>
						</div>
					<?php } ?>
					
					<p>
						<?php if(get_the_author_meta('url')) { the_author_meta('display_name'); ?>'s <a href="<?php the_author_meta( 'url'); ?>" target="_blank" rel="nofollow">Webseite</a>. <?php } ?>
						<?php if(get_the_author_meta('facebook')) { the_author_meta('display_name'); ?> auf <a href="<?php the_author_meta( 'facebook'); ?>" target="_blank" rel="nofollow">Facebook</a>. <?php } ?>
						<?php if(get_the_author_meta('googleplus')) { the_author_meta('display_name'); ?> auf <a href="<?php the_author_meta( 'googleplus'); ?>" target="_blank" rel="nofollow">Google+</a>. <?php } ?>
						<?php if(get_the_author_meta('twitter')) { the_author_meta('display_name'); ?> auf <a href="https://twitter.com/<?php the_author_meta( 'twitter'); ?>" target="_blank" rel="nofollow">Twitter</a>. <?php } ?>
					</p>
					
					<hr>
					
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<?php get_template_part('parts/post/loop', xcore_get_post_layout('author')); ?>
					<?php endwhile; echo pagination(3); endif; ?>
				</div>
			</div>
			
			<?php if($sidebar == 'right') { ?>
				<div class="col-sm-4">
					<div id="sidebar">
						<?php get_sidebar(); ?>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
