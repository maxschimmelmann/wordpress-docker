<?php 
get_header(); 

/*
 * VARS
 */
$sidebar = xcore_get_sidebar('home');
$posts_page_id = get_option( 'page_for_posts');
?>

<div id="main" class="<?php echo get_section_layout_class('content'); ?>">
	<div class="container">
		<div class="row">
			<?php if($sidebar == 'left') { ?>
				<div class="col-sm-4">
					<div id="sidebar">
						<?php get_sidebar(); ?>
					</div>
				</div>
			<?php } ?>
						
			<div class="col-sm-<?php if($sidebar == 'none') : echo '12'; else: echo '8'; endif; ?>">
				<div id="content">
					<?php if(!is_paged() && get_post_field('post_content', $posts_page_id)) echo apply_filters('the_content', get_post_field('post_content', $posts_page_id)) . '<hr>'; ?>
				
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<?php get_template_part('parts/post/loop', xcore_get_post_layout('home')); ?>
					<?php endwhile; echo pagination(3); endif; ?>
				</div>
			</div>
			
			<?php if($sidebar == 'right') { ?>
				<div class="col-sm-4">
					<div id="sidebar">
						<?php get_sidebar(); ?>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
