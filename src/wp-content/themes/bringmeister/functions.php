<?php
/*
 * xCORE Framework Functions Library - Dont touch this. Use Childthemes!
 * 
 * @author		Christian Lang
 * @version		1.0
 * @category	functions
 */
define( 'ENDCORE_LIBRARY', TEMPLATEPATH . '/library' );
define( 'ENDCORE_HELPER', TEMPLATEPATH . '/library/helper' );
define( 'ENDCORE_PLUGINS', TEMPLATEPATH . '/library/plugins' );

/* 
 * Add-Ons
 */
require_once(ENDCORE_PLUGINS . '/acf/core/acf.php');
require_once(ENDCORE_PLUGINS . '/acf/field-type-autocomplete/acf-field-type-autocomplete.php');
require_once(ENDCORE_PLUGINS . '/acf/field-type-code/acf-code_area.php');
require_once(ENDCORE_PLUGINS . '/acf/field-type-selector/acf-field_selector.php');
require_once(ENDCORE_PLUGINS . '/acf/acf-functions.php');
require_once(ENDCORE_PLUGINS . '/tinymce/excerpt.php');
require_once(ENDCORE_PLUGINS . '/tinymce/shortcodes-generator.php');
require_once(ENDCORE_PLUGINS . '/mobile-detect/Mobile_Detect.php');

/*
 * Framework Funktionen
 */
require_once(ENDCORE_LIBRARY . '/helper/_load.php');
require_once(ENDCORE_LIBRARY . '/navigation/_load.php');
require_once(ENDCORE_LIBRARY . '/widgets/_load.php');
require_once(ENDCORE_LIBRARY . '/job/_load.php');
?>