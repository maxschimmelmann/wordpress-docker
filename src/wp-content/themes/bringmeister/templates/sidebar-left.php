<?php 
/*
 * TEMPLATE NAME: Sidebar (Links)
 */
get_header(); ?>

<div id="main" class="<?php echo get_section_layout_class('content'); ?>">
	<div class="container">
		<div class="row">
			<div class="col-lg-9 col-lg-push-3 col-md-9 col-md-push-3 col-sm-8 col-sm-push-4">
				<div id="content">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<?php the_content(); ?>
						
						<?php 
						if(xcore_get_social('page'))
							get_template_part('parts/stuff/code', 'social');
						?>
					<?php endwhile; endif; ?>
				</div>
			</div>
			
			<div class="col-lg-3 col-lg-pull-9 col-md-3 col-md-pull-9 col-sm-4 col-sm-pull-8">
				<div id="sidebar">
					<?php get_sidebar(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>
