<?php
/*
 * Template Name: Page Builder
 */
get_header(); ?>

    <div id="main" class="<?php echo get_section_layout_class('content'); ?>">
		<div id="page-builder">
			<?php
			if( have_rows('page_builder') ):
				$i=0;
				$output = '';

				while ( have_rows('page_builder') ) : the_row();
					/**
					 * Feld: Textarea
					 */
					if( get_row_layout() == 'page_builder_textarea' ):
						$bgimage = get_sub_field('bgimage');

						$attributes = array(
							'class' => array('section', 'textarea', 'textarea-row-' . get_sub_field('rows'), 'id-' . $i),
							'style' => array(),
						);

						if(get_sub_field('id')) {
							$attributes['id'][] = get_sub_field('id');
						}

						if(get_sub_field('padding') == '1') {
							$attributes['class'][] = 'no-padding';
						}

						if(get_sub_field('class')) {
							$attributes['class'][] = get_sub_field('class');
						}

						$offset = get_sub_field('offset');

						if(get_sub_field('bgcolor')) {
							$attributes['style'][] = 'background-color: ' . get_sub_field('bgcolor') . ';';

							if(get_sub_field('bgcolor') == '#0c617a') {
								$attributes['class'][] = 'text-white';
							}
						}

						if($bgimage) {
							$attributes['class'][] = 'has-background';
							$attributes['style'][] = 'background-image:url(' . $bgimage['url'] . '); background-repeat: no-repeat; background-position-y: bottom;';
						}

						$output .= '<div ' . xcore_attribute_array_html($attributes) . '>';
							$output .= '<div class="container">';
								$items = get_sub_field('editor');
								if($items) {
									$count = count($items);

									$output .= '<div class="row' . (get_sub_field('eq') == '1' ? ' col-eq' : ''). '">';

									foreach($items as $item) {
										$output .= xcore_pb_render_editor($item, $count, $i, $offset);
									}

									$output .= '</div>';
								}
							$output .= '</div>';
						$output .= '</div>';
					endif;

					/**
					 * Feld: Slideshow
					 */
					if(get_row_layout() == 'page_builder_slideshow'):
						global $indicator, $arrows, $interval, $fade, $images;
						$indicator = get_sub_field('indicator');
						$arrows = get_sub_field('arrows');
						$fade = get_sub_field('fade');
						$interval = get_sub_field('interval');
						$images = get_sub_field('images');

						$attributes = array(
							'class' => array('section', 'slideshow', 'id-' . $i),
							'style' => array(),
						);

						if(get_sub_field('id')) {
							$attributes['id'][] = get_sub_field('id');
						}

						if(get_sub_field('class')) {
							$attributes['class'][] = get_sub_field('class');
						}

						if($images) {
							$output .= '<div ' . xcore_attribute_array_html($attributes) . '>';
								$output .= '<div class="container">';
									ob_start();
									get_template_part('parts/teaser/code', 'teaser');
									$output .= ob_get_contents();
									ob_end_clean();
								$output .= '</div>';
							$output .= '</div>';
						}
					endif;

					/**
					 * Feld: Box mit Bild und Text (Caption)
					 */
					if( get_row_layout() == 'page_builder_box_caption' ):
						$bgimage = get_sub_field('bgimage');
						$layout = get_sub_field('layout');

						$attributes = array(
							'class' => array('section', 'textarea', 'textarea-row-' . get_sub_field('rows'), 'textarea-caption', 'id-' . $i),
							'style' => array(),
						);

						if(get_sub_field('id')) {
							$attributes['id'][] = get_sub_field('id');
						}

						if(get_sub_field('padding') == '1') {
							$attributes['class'][] = 'no-padding';
						}

						if(get_sub_field('class')) {
							$attributes['class'][] = get_sub_field('class');
						}

						$offset = get_sub_field('offset');

						if(get_sub_field('bgcolor')) {
							$attributes['style'][] = 'background-color: ' . get_sub_field('bgcolor') . ';';

							if(get_sub_field('bgcolor') == '#0c617a') {
								$attributes['class'][] = 'text-white';
							}
						}

						if($bgimage) {
							$attributes['class'][] = 'has-background';
							$attributes['style'][] = 'background-image:url(' . $bgimage['url'] . '); background-repeat: no-repeat; background-position-y: bottom;';
						}

						$output .= '<div ' . xcore_attribute_array_html($attributes) . '>';
							$output .= '<div class="container">';
								$items = get_sub_field('editor');
								if($items) {
									$count = count($items);

									$output .= '<div class="row caption-eq">';

									$c=1;
									foreach($items as $item) {
										$output .= xcore_pb_render_editor($item, $count, $c, $offset, true, $layout);

										$c++;
									}

									$output .= '</div>';
								}
							$output .= '</div>';
						$output .= '</div>';
					endif;

					 /**
					 * Feld: Jobs
					 */
					if( get_row_layout() == 'page_builder_job' ):
						$bgimage = get_sub_field('bgimage');
						$layout = get_sub_field('layout');

						$attributes = array(
							'class' => array('section', 'jobs'),
							'style' => array(),
						);

						if(get_sub_field('id')) {
							$attributes['id'][] = get_sub_field('id');
						}

						if(get_sub_field('class')) {
							$attributes['class'][] = get_sub_field('class');
						}

						if(get_sub_field('bgcolor')) {
							$attributes['style'][] = 'background-color: ' . get_sub_field('bgcolor') . ';';
						}

						if($bgimage) {
							$attributes['class'][] = 'has-background';
							$attributes['style'][] = 'background-image:url(' . $bgimage['url'] . '); background-repeat: no-repeat; background-position-y: bottom;';
						}

						$output .= '<div ' . xcore_attribute_array_html($attributes) . '>';
							$output .= '<div class="container">';
								$output .= '<div class="inner">';
									$headline = get_sub_field('headline');

									if($headline) {
										$output .= '<h2 class="section-headline text-center">' . $headline . '</h2>';
									}

									$args = array(
										'post_type' => 'job',
										'posts_per_page' => 5,
									);

									if($job_ids = get_sub_field('jobs')) {
										$args['post__in'] = $job_ids;
										$args['posts_per_page'] = -1;
									}

									$jobs = new WP_Query($args);

									if($jobs->have_posts()) {
										$output .= '<div class="row">';
											while($jobs->have_posts()) {
												$jobs->the_post();

												ob_start();
												get_template_part('parts/job/loop', 'grid');
												$output .= ob_get_contents();
												ob_end_clean();
											}

											$count_jobs = wp_count_posts('job');
											if($count_jobs->publish-5 > 0) {
												$job_standort = get_terms('job_standort', 'hide_empty=1');

												$output .= '<div class="col-sm-6">';
													$output .= '<div class="job-more">';
														$output .= '<p class="h4">' . sprintf(__('%s weitere Jobs...'), $count_jobs->publish-5) . '</p>';
														if($job_standort) {
															$count_standorte = count($job_standort);

															$output .= '<p>' . __('an den Standorten ', 'xcore');
																$i=1;
																foreach($job_standort as $standort) {
																	if($i==1) {
																		$output .= $standort->name;
																	}

																	if($i>1 && $i<$count_standorte) {
																		$output .= ', ' . $standort->name;
																	}

																	if($i==$count_standorte) {
																		$output .= ' und ' . $standort->name;
																	}

																	$i++;
																}
															$output .= '</p>';
														}
														$output .= '<p><a href="/jobs/" class="btn btn-xcore btn-arrow-right btn-lg">' . __('Zeige alle Jobs!', 'xcore')  . '</a></p>';
													$output .= '</div>';
												$output .= '</div>';
											}
										$output .= '</div>';

										wp_reset_postdata();
									}
								$output .= '</div>';
							$output .= '</div>';
						$output .= '</div>';
					endif;

					/**
					 * Feld: Trennlinie
					 */
					if(get_row_layout() == 'page_builder_hr'):
						$layout = get_sub_field('layout');

						$element_attributes = array(
							'style' => array(),
						);

						$attributes = array(
							'class' => array('section', 'hr', 'id-' . $i),
							'style' => array(),
						);

						if(get_sub_field('id')) {
							$attributes['id'][] = get_sub_field('id');
						}

						if(get_sub_field('class')) {
							$attributes['class'][] = get_sub_field('class');
						}

						if(get_sub_field('size')) {
							$element_attributes['class'][] = get_sub_field('size');
						}

						if(get_sub_field('color')) {
							$element_attributes['style'][] = 'border-color: ' . get_sub_field('color') . ';';
						}

						$output .= '<div ' . xcore_attribute_array_html($attributes) . '>';
							$output .= '<div class="container">';
								$output .= '<hr ' . xcore_attribute_array_html($element_attributes) . '>';
							$output .= '</div>';
						$output .= '</div>';
					endif;

					$i++;
				endwhile;

				echo apply_filters('pb_the_content', do_shortcode($output));
			endif;

			if(xcore_get_social('page'))
				get_template_part('parts/stuff/code', 'social');
			?>
        </div>
    </div>

<?php get_footer(); ?>