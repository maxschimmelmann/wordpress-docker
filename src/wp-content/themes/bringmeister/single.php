<?php 
get_header(); 

/*
 * VARS
 */
$sidebar = xcore_get_sidebar('single');
?>

<div id="main" class="<?php echo get_section_layout_class('content'); ?>">
	<div class="container">
		<div class="row">
			<?php if($sidebar == 'left') { ?>
				<div class="col-sm-4">
					<div id="sidebar">
						<?php get_sidebar(); ?>
					</div>
				</div>
			<?php } ?>
						
			<div class="col-sm-<?php if($sidebar == 'none') : echo '12'; else: echo '8'; endif; ?>">
				<div id="content">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<article <?php post_class(); ?> role="article">
							<h1><?php the_title(); ?></h1>
							
							<?php
							if('1' == get_field('blog_single_show_meta', 'option'))
								get_template_part('parts/stuff/code', 'postmeta');
							 
							if(xcore_get_social('blog') && ('top' == xcore_get_social_pos('blog') || 'both' == xcore_get_social_pos('blog')))
								get_template_part('parts/stuff/code', 'social');
							?>
							
							<?php the_content(); ?>
							
							<div class="clearfix"></div>
						</article>
						
						<?php
						if(xcore_get_social('blog') && ('bottom' == xcore_get_social_pos('blog') || 'both' == xcore_get_social_pos('blog')))
							get_template_part('parts/stuff/code', 'social');
						
						if('1' == get_field('blog_single_show_authorbox', 'option'))
							get_template_part('parts/stuff/code', 'author');
						
						if('1' == get_field('blog_single_show_related', 'option'))
							get_template_part('parts/stuff/code', 'related');
						
						if('1' == get_field('blog_single_show_postnav', 'option'))
							get_template_part('parts/stuff/code', 'postnav');
						
						get_template_part('parts/stuff/code', 'comments'); 
						?>
					<?php endwhile; endif; ?>
				</div>
			</div>
			
			<?php if($sidebar == 'right') { ?>
				<div class="col-sm-4">
					<div id="sidebar">
						<?php get_sidebar(); ?>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
