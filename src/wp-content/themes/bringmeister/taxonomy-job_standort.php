<?php
get_header();

/*
 * VARS
 */
$sidebar = 'left';
$queried_object = get_queried_object();
$term_id = $queried_object->term_id;
?>

<div id="main" class="<?php echo get_section_layout_class('content'); ?>">
    <div class="container">
        <div class="row">
            <?php if($sidebar == 'left') { ?>
                <div class="col-sm-3">
                    <div id="sidebar">
                        <?php get_sidebar(); ?>
                    </div>
                </div>
            <?php } ?>

            <div class="col-sm-<?php if($sidebar == 'none') : echo '12'; else: echo '9'; endif; ?>">
                <div id="content">
                    <h1><?php _e('Jobangebote', 'xcore'); ?></h1>

                    <div class="jobs">
                        <?php
                        $jobs_found = false;

                        $terms = get_terms('job_bereich', 'hide_empty=1');

                        if($terms) {
                            $i=0;
                            foreach($terms as $term) {
                                $args = array(
                                    'post_type' => 'job',
                                    'posts_per_page' => -1,
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'job_standort',
                                            'field' => 'term_id',
                                            'terms' => array($term_id)
                                        ),
                                        array(
                                            'taxonomy' => 'job_bereich',
                                            'field' => 'term_id',
                                            'terms' => array($term->term_id)
                                        )
                                    )
                                );

                                $jobs = new WP_Query($args);

                                if($jobs->have_posts()) {
                                    $jobs_found = true;

                                    if($i==0) {
                                        if(single_cat_title(false, false) == "Berlin") {
                                            printf(__('<p>Unsere Jobs – mitten in %s:</p>', 'xcore'), single_cat_title(false, false));
                                        } else {
                                            printf(__('<p>Packende Jobs in %s:</p>', 'xcore'), single_cat_title(false, false));
                                        }
                                    }

                                    echo '<div class="jobs-section">';
                                        echo '<h2>' . $term->name . '</h2>';
                                        echo '<ul class="list list-unstyled list-jobs">';
                                            while($jobs->have_posts()) {
                                                $jobs->the_post();

                                                echo '<li><a href="' . get_permalink() . '">' . get_the_title() . ' <span class="badge badge-job">' . strip_tags(get_the_term_list($post->ID, 'job_bereich', '', ', ', '')) . '</span></a></li>';
                                            }
                                        echo '</ul>';
                                    echo '</div>';

                                    $i++;
                                }
                            }
                        }

                        if($jobs_found == false) {
                            ?>
                            <p><?php printf(__('Derzeit sind alle unsere Stellen am Standort %s besetzt', 'xcore'), single_cat_title(false, false)); ?></p>

                            <div class="job-contact">
                                <p class="h4 gray"><?php _e(' Du bist hungrig nach neuen Aufgaben und möchtest unser Team tatkräftig unterstützen?', 'xcore'); ?></p>
                                <p class="h4"><?php _e('Dann freuen wir uns über Deine Initiativbewerbung.', 'xcore'); ?></p>
                                <p>&nbsp;</p>
                                <p><strong><?php _e('Cindy Hoch', 'xcore'); ?></strong><br><?php _e('HR Managerin', 'xcore'); ?></p>
                                <p><a href="mailto:bewerbung@bringmeister.de" class="btn btn-white btn-lg btn-arrow-right">bewerbung@bringmeister.de</a></p>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>

            <?php if($sidebar == 'right') { ?>
                <div class="col-sm-3">
                    <div id="sidebar">
                        <?php get_sidebar(); ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
