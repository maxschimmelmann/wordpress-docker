		<?php if ( function_exists('yoast_breadcrumb') && ('above_footer' == get_field('design_breadcrumbs_pos', 'option'))) {
			?>
			<section id="breadcrumbs" class="<?php echo get_section_layout_class('breadcrumbs'); ?>">
				<div class="container">
					<?php
					yoast_breadcrumb('<p>','</p>');
					?>
				</div>
			</section>
			<?php
		} ?>

		<footer id="footer" class="<?php echo get_section_layout_class('footer'); ?>">
			<div id="footer-symbols">
				<img src="<?php echo get_template_directory_uri(); ?>/_/img/footer-symbole.png">
			</div>
			<?php
            get_template_part('parts/footer/code', 'top');

			//get_template_part('parts/footer/code', 'bottom');
			?>
		</footer>

		<?php wp_footer(); ?>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		</div>
	</body>
</html>