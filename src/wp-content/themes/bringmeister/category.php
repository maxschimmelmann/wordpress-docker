<?php 
get_header(); 

/*
 * VARS
 */
$sidebar = xcore_get_sidebar('category');
?>

<div id="main" class="<?php echo get_section_layout_class('content'); ?>">
	<div class="container">
		<div class="row">
			<?php if($sidebar == 'left') { ?>
				<div class="col-sm-4">
					<div id="sidebar">
						<?php get_sidebar(); ?>
					</div>
				</div>
			<?php } ?>
						
			<div class="col-sm-<?php if($sidebar == 'none') : echo '12'; else: echo '8'; endif; ?>">
				<div id="content">
					<h1><?php single_cat_title(); ?></h1>
					
					<?php if(category_description() && !is_paged()) { echo category_description() . '<hr>'; } ?>
										
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<?php get_template_part('parts/post/loop', xcore_get_post_layout('category')); ?>
					<?php endwhile; echo pagination(3); endif; ?>
				</div>
			</div>
			
			<?php if($sidebar == 'right') { ?>
				<div class="col-sm-4">
					<div id="sidebar">
						<?php get_sidebar(); ?>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
