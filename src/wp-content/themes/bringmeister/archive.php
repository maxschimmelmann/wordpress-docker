<?php 
get_header(); 

/*
 * VARS
 */
$sidebar = xcore_get_sidebar('archive');
?>

<div id="main" class="<?php echo get_section_layout_class('content'); ?>">
	<div class="container">
		<div class="row">
			<?php if($sidebar == 'left') { ?>
				<div class="col-sm-4">
					<div id="sidebar">
						<?php get_sidebar(); ?>
					</div>
				</div>
			<?php } ?>
						
			<div class="col-sm-<?php if($sidebar == 'none') : echo '12'; else: echo '8'; endif; ?>">
				<div id="content">
					<h1>
						<?php
							if ( is_day() ) :
								printf( __( 'Tagesarchive: %s', 'xcore' ), get_the_date() );
							elseif ( is_month() ) :
								printf( __( 'Monatsarchive: %s', 'xcore' ), get_the_date( _x( 'F Y', 'monatliches datum format', 'xcore' ) ) );
							elseif ( is_year() ) :
								printf( __( 'Jahresarchive: %s', 'xcore' ), get_the_date( _x( 'Y', 'jaehrliches datum format', 'xcore' ) ) );
							else :
								_e( 'Archive', 'xcore' );
							endif;
						?>
					</h1>
					
					<hr>
									
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<?php get_template_part('parts/post/loop', xcore_get_post_layout('archive')); ?>
					<?php endwhile; echo pagination(3); endif; ?>
				</div>
			</div>
			
			<?php if($sidebar == 'right') { ?>
				<div class="col-sm-4">
					<div id="sidebar">
						<?php get_sidebar(); ?>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
