<?php
/*
 * Registrieren der Navigationen
 * 
 * @author		Christian Lang
 * @version		1.0
 * @category	navigation
 */

add_action('init', 'xcore_register_menu');
function xcore_register_menu() {
	register_nav_menu('nav_main', __('Hauptnavigation', 'xcore'));
	register_nav_menu('nav_footer', __('Footer', 'xcore'));
}