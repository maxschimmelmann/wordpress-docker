<?php
/**
 * Widget: Navigation
 *
 * @author		Christian Lang
 * @version		1.0
 * @category	widgets
 */

class xcore_navigation_widget extends WP_Widget {
    public function __construct() {
        $widget_ops = array('classname' => 'widget_navigation', 'description' => __('Dieses Widget zeigt die Städte an.', 'xcore'));
        parent::__construct('xcore_navigation_widget', __('bringmeister.de &raquo; Städte Navigation', 'xcore'), $widget_ops);
    }

    function widget($args, $instance) {
        extract($args, EXTR_SKIP);
        global $post;

        echo $before_widget;

        $terms = get_terms('job_standort', 'hide_empty=0');

        if($terms) {
            $term_id = 0;

            if(is_tax()) {
                $queried_object = get_queried_object();
                if ($queried_object) {
                    $term_id = $queried_object->term_id;
                }
            }

            if(is_singular('job')) {
                $job_bereiche = wp_get_post_terms($post->ID, 'job_standort', array("fields" => "ids"));

                if($job_bereiche) {
                    $term_id = $job_bereiche[0];
                }
            }

	        //echo '<p class="h1"><a href="/jobs/">' . __('Unsere Standorte', 'xcore') . '</a></p>';

            echo '<ul class="list-terms">';
                foreach($terms as $term) {
                    echo '<li class="' . ($term_id == $term->term_id ? 'active' : '') . '"><a href="' . get_term_link($term) . '" title="' . $term->name . '">' . $term->name . ' <span>' . $term->count . '</span></a></li>';
                }
            echo '</ul>';
        }

        echo $after_widget;
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        return $instance;
    }

    function form($instance) {
        $instance = wp_parse_args( (array) $instance, array(
            )
        );
    }
}

add_action( 'widgets_init', 'xcore_navigation_widget' );
function xcore_navigation_widget() {
    register_widget('xcore_navigation_widget');
}