<?php
/*
 * Custom Class für jedes Widget (auch Core Widgets)
 * 
 * @author		Christian Lang
 * @version		1.0
 * @category	widgets
 */
 
add_action('in_widget_form', 'xcore_in_widget_form',5,3);
function xcore_in_widget_form($t,$return,$instance){
	$instance = wp_parse_args( (array) $instance, array('xcore_class' => '') );
	if ( !isset($instance['xcore_class']) )
		$instance['xcore_class'] = null;
	?>
	<p>
		<label for="<?php echo $t->get_field_id('xcore_class'); ?>"><?php _e('CSS-Klasse:', 'xcore'); ?></label><br>
		<input type="text" class="widefat" id="<?php echo $t->get_field_id('xcore_class'); ?>" name="<?php echo $t->get_field_name('xcore_class'); ?>" value="<?php echo $instance['xcore_class'];?>" />
	</p>
	<?php
	$return = null;
	return array($t,$return,$instance);
}

add_filter('widget_update_callback', 'xcore_in_widget_form_update',5,3);
function xcore_in_widget_form_update($instance, $new_instance, $old_instance){
	$instance['xcore_class'] = $new_instance['xcore_class'];
	return $instance;
}

add_filter('dynamic_sidebar_params', 'xcore_dynamic_sidebar_params');
function xcore_dynamic_sidebar_params($params){
	global $wp_registered_widgets;
	$widget_id = $params[0]['widget_id'];
	$widget_obj = $wp_registered_widgets[$widget_id];
	$widget_opt = get_option($widget_obj['callback'][0]->option_name);
	$widget_num = $widget_obj['params'][0]['number'];
	
	if(isset($widget_opt[$widget_num]['xcore_class']))
		$xcore_class = $widget_opt[$widget_num]['xcore_class'];
	else
		$xcore_class = '';
	$params[0]['before_widget'] = preg_replace('/class="/', 'class="' . $xcore_class . ' ',  $params[0]['before_widget'], 1);
	
	return $params;
}