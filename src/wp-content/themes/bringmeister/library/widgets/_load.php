<?php
/*
 * Laden der Hilfsfunktionen
 * 
 * @author		Christian Lang
 * @version		1.0
 * @category	_load
 */

require_once(ENDCORE_LIBRARY . '/widgets/areas.php');
require_once(ENDCORE_LIBRARY . '/widgets/class.php');
require_once(ENDCORE_LIBRARY . '/widgets/navigation-locations.php');