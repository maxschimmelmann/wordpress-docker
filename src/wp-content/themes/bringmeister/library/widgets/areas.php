<?php
/*
 * Registrieren der Sidebars
 * 
 * @author		Christian Lang
 * @version		1.0
 * @category	widgets
 */
 
register_sidebar(array(
	'name' => __('Allgemeine Sidebar', 'xcore'),
	'id' => 'standard',
	'description' => __('Allgemeine Sidebar, diese ist fuer WooSidebars notwendig.', 'xcore'),
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => '</aside>',
	'before_title' => '<p class="h1">',
	'after_title' => '</p>',
));

if(get_field('design_footer_widgets', 'option') == '1') {
	for($i=1; $i<4; $i++) {
		register_sidebar(array(
			'name' => __('Footer ', 'xcore') . '('.$i.')',
			'id' => 'footer_'.$i,
			'description' => '',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<p class="h1">',
			'after_title' => '</p>',
		));
	}
}
?>