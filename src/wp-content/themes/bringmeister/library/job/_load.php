<?php
/*
 * Laden der Hilfsfunktionen
 *
 * @author		Christian Lang
 * @version		1.0
 * @category	_load
 */
require_once('helper.php');
require_once('posttype.php');
require_once('taxonomy.php');