<?php
/*
 * Post Type: Jobs
 *
 * @author		Christian Lang
 * @version		1.0
 * @category	job
 */
add_action('init', 'job_register');
function job_register() {
    $labels = array(
        'name' 				=> __('Jobangebote', 'xcore'),
        'singular_name' 	=> __('Jobangebot', 'xcore'),
        'add_new' 			=> __('Neuen Eintrag Erstellen', 'xcore'),
        'add_new_item' 		=> __('Neuen Eintrag hinzuf&uuml;gen', 'xcore'),
        'edit_item' 		=> __('Eintrag editieren', 'xcore'),
        'new_item' 			=> __('Neuer Eintrag', 'xcore'),
        'all_items' 		=> __('Eintr&auml;ge', 'xcore'),
        'view_item' 		=> __('Zeige Eintrag', 'xcore'),
        'search_items' 		=> __('Suche Eintrag', 'xcore'),
        'not_found' 		=> __('Kein Eintrag gefunden', 'xcore'),
        'menu_name' 		=> __('Jobangebote', 'xcore') // DKB: fixed typo
    );

    $args = array(
        'labels'			=> $labels,
        'public'			=> true,
        'show_ui'			=> true,
        'hierarchical'		=> true,
        'has_archive'		=> false,
        'menu_icon'			=> 'dashicons-nametag',
        'rewrite'			=> array('slug' => 'job', 'with_front' => false),
        'supports' 			=> array('title', 'editor', 'thumbnail', 'author', 'custom-fields')
    );

    register_post_type('job', $args);
}