<?php
/*
 * Taxonomy
 *
 * @author		Christian Lang
 * @version		1.0
 * @category	job
 */

add_action( 'init', 'job_taxonomies', 0 );
function job_taxonomies() {
    register_taxonomy(
        'job_bereich',
        'job',
        array(
            'label' => __('Bereich', 'xcore'),
            'rewrite' => array( 'slug' =>  'job-bereich', 'with_front' => true, 'hierarchical' => true),
            'hierarchical' => true,
            'query_var' => true,
            'sort' => true,
            'public' => true,
        )
    );

    register_taxonomy(
        'job_standort',
        'job',
        array(
            'label' => __('Standort', 'xcore'),
            'rewrite' => array( 'slug' =>  'standort', 'with_front' => true, 'hierarchical' => true),
            'hierarchical' => true,
            'query_var' => true,
            'sort' => true,
            'public' => true,
        )
    );

    flush_rewrite_rules();
}