<?php
/*
 * Hilfsfunktionen
 *
 * @author		Christian Lang
 * @version		1.0
 * @category	job
 */

add_action('wp_ajax_job_filter', 'xcore_job_filter');
add_action('wp_ajax_nopriv_job_filter', 'xcore_job_filter');
function xcore_job_filter() {
    $args = array(
        'post_type' => 'job',
        'posts_per_page' => -1,
        'tax_query' => array()
    );

    // taxonomy filter
    foreach($_POST as $k => $v) {
        if(strpos($k, 'job_') !== false) {
            $args['tax_query'][$k] = array(
                'taxonomy' => $k,
                'field' => 'ids',
                'terms' => $v
            );
        }
    }

    $jobs = new WP_Query($args);

    if($jobs->have_posts()) {
        while($jobs->have_posts()) {
            $jobs->the_post();

            get_template_part('parts/job/loop', 'list');
        }

        wp_reset_query();
    } else {
        ?>
        <p><?php _e('Für Ihre Suche wurden keine Jobs oder Praktikas gefunden.</p>', 'xcore'); ?></p>
        <?php
    }

    exit;
}

add_filter( 'nav_menu_css_class', 'xcore_job_menu_item_classes', 10, 4 );
function xcore_job_menu_item_classes( $classes, $item, $args, $depth ) {
    if('nav_main' !== $args->theme_location)
        return $classes;

    if((is_singular('job')) && 472 == $item->ID )
        $classes[] = 'current_page_item';

    return array_unique( $classes );
}

/**
 * Gravity Forms Validation Message
 */
add_filter("gform_validation_message", "xcore_gf_error_message", 10, 2);
function xcore_gf_error_message($message, $form){
    // check failed fields
    $message = '<div class="alert alert-danger">';
        $message .= '<h3>Bitte überprüfen Sie Ihre Angaben.</h3>';

        $fields = $form['fields'];
        if($fields) {
            foreach($fields as $field) {
                if($field->failed_validation) {
                    if($field->errorMessage) {
                        $message .= $field->errorMessage . '<br>';
                    }
                }
            }
        }
    $message .= '</div>';

    return $message;
}
