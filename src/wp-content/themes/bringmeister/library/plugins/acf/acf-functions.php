<?php
/*
 * ACF Funktionen
 * 
 * @author		Christian Lang
 * @version		1.0
 * @category	acf
 */

define( 'ACF_LITE' , false );

add_filter('acf/settings/path', 'xcore_acf_settings_path');
function xcore_acf_settings_path( $path ) {
	$path = ENDCORE_PLUGINS . '/acf/core/';
	return $path;
	
}

add_filter('acf/settings/dir', 'xcore_acf_settings_dir');
function xcore_acf_settings_dir( $dir ) {
	$dir = get_template_directory_uri() . '/library/plugins/acf/core/';
	return $dir;
}

add_filter('acf/settings/load_json', 'xcore_acf_json_load_point');
function xcore_acf_json_load_point( $paths ) {
	unset($paths[0]);
	$paths[] = get_template_directory() . '/library/plugins/acf/json';
	return $paths;
}

add_filter('acf/settings/save_json', 'xcore_acf_json_save_point');
function xcore_acf_json_save_point( $path ) {
	$path = get_template_directory() . '/library/plugins/acf/json';
	return $path;
}

/*
 * Update-Vorgang deaktivieren
 */
add_filter('acf/settings/show_updates', 'xcore_acf_settings_show_updates');
function xcore_acf_settings_show_updates( $path ) {
	return false;
}

if(function_exists('acf_add_options_page')) { 
	acf_add_options_page(array('page_title' => 'Framework', 'menu_title' => 'Framework', 'icon_url' => 'dashicons-heart', 'capability' => 'manage_options'));
	acf_add_options_sub_page(array('title' => 'Allgemein', 'parent' => 'acf-options-framework'));
	acf_add_options_sub_page(array('title' => 'Design', 'parent' => 'acf-options-framework'));
	acf_add_options_sub_page(array('title' => 'Blog', 'parent' => 'acf-options-framework'));
	
	acf_add_options_page(array('page_title' => 'Optionen', 'menu_title' => 'Optionen', 'icon_url' => '', 'capability' => 'manage_options'));
    acf_add_options_sub_page(array('title' => 'Header', 'parent' => 'acf-options-optionen'));
    //acf_add_options_sub_page(array('title' => 'Footer', 'parent' => 'acf-options-optionen'));

}

add_filter('acf/fields/flexible_content/layout_title', 'xcore_acf_custom_fc_title', 10, 4);
function xcore_acf_custom_fc_title($title, $field, $layout, $i) {
	global $post;

	$post_id = $post->ID;

	if(isset($_POST['post_id'])) $post_id = $_POST['post_id'];

	$label = get_field('page_builder_' . $i . '_label', $post_id);

	if($label) {
		return $title . ': ' . $label;
	}

	return $title;
}

?>