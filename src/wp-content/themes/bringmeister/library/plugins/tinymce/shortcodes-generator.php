<?php
/*
 * Shortcode Generator
 * 
 * @author		Christian Lang
 * @version		1.0
 * @category	tinymce
 */

add_action('admin_head', 'endcore_addbuttons');
function endcore_addbuttons() {
	global $typenow;

	if ( !current_user_can('edit_posts') && !current_user_can('edit_pages') ) {
		return;
	}

	if ( get_user_option('rich_editing') == 'true') {
		add_filter("mce_external_plugins", "add_endcore_shortcodes_tinymce_plugin");
		add_filter('mce_buttons', 'register_endcore_shortcodes_button');
	}
}
function add_endcore_shortcodes_tinymce_plugin($plugin_array) {
	$plugin_array['endcore_shortcodes_button'] = esc_url( get_template_directory_uri() ) . '/library/plugins/tinymce/shortcodes.js';
	return $plugin_array;
}
function register_endcore_shortcodes_button($buttons) {
	array_push($buttons, "endcore_shortcodes_button");
	return $buttons;
}
?>