<?php
/*
 * Bringmeister Functions
 *
 * @author		Christian Lang
 * @version		1.0
 * @category	helper
 */

function xcore_sm_current_location() {
    global $post;

    $infos = array(
        'short' => 'ber',
        'sub' => 'berlin',
        'label' => 'Berlin',
        'url' => 'https://berlin.bringmeister.de',
        'nav' => array(
            'shop' => 'nav_main_ber',
            'themenwelt' => 'nav_page_ber'
        ),
        'zipcode_base' => '1'
    );

    if(!$post) return $infos;

    $post_parents = get_post_ancestors($post->ID);
    if(in_array(25835, $post_parents) || $post->ID == 25835) {
        $infos = array(
            'short' => 'muc',
            'sub' => 'muc',
            'label' => 'München',
            'url' => 'https://muenchen.bringmeister.de',
            'nav' => array(
                'shop' => 'nav_main_muc',
                'themenwelt' => 'nav_page_muc'
            ),
            'zipcode_base' => '8'
        );
    }

    return $infos;
}

function xcore_sm_shop() {
    return 'https://bringmeister.de';
}

function xcore_sm_generate_link($path) {
    return xcore_sm_shop() . '/' . $path;
}

function xcore_sm_nav_links($links) {
    $links_arr = array();
    $output = '';

    if($links) {
        foreach($links as $k => $v) {
            $name = $v['name'];
            $url = xcore_sm_generate_link($v['browserUrl']);

            $links_arr[] = '<a href="' . $url . '" target="_blank">' . $name . '</a>';
        }

        $output = implode(', ', $links_arr);
    }

    return $output;
}

function xcore_sm_shop_search() {
    $location = xcore_sm_current_location();
    ?>
    <form role="search" method="get" id="searchform" class="searchform" action="<?php echo $location['url']; ?>/catalogsearch/result/?">
        <div class="input-group">
            <input type="text" class="form-control" name="q" id="name" placeholder="<?php echo __('Suche' , 'xcore'); ?>" value="<?php echo get_search_query(); ?>">
            <span class="input-group-btn">
			<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
		</span>
        </div>
    </form>
    <?php
}

function xcore_sm_get_categories() {
    if ( false === ( $items = get_transient( 'api_categories' ) ) ) {
        $items = file_get_contents(ABSPATH . 'api/categories/items.json');
        set_transient( 'api_categories', $items, 24 * HOUR_IN_SECONDS );
    }

    if($items) {
        $items_arr = json_decode($items, true);

        return $items_arr['categories'];
    }

    return array();
}

function xcore_sm_generate_category_navigation($items) {
    $output = '';

    if($items) {
        ob_start();

        foreach($items as $item_main) {
            $name = $item_main['name'];
            $url = xcore_sm_generate_link($item_main['browserUrl']);
            $background = $item_main['imageUrl'];
            $nav_items_sub = $item_main['children'];

            if($name) {
                $li_attr = ($nav_items_sub ? 'class="dropdown dropdown-submenu" ' : '');
                $a_attr = ($nav_items_sub ? 'class="dropdown-toggle" ' : '');
                ?>
                <li <?php echo $li_attr; ?>>
                    <a <?php echo $a_attr; ?>href="<?php echo ($url ? $url : '#'); ?>" rel="nofollow">
                        <?php echo $name; ?>
                    </a>

                    <?php if($nav_items_sub) { ?>
                        <ul class="dropdown-menu" style="background-image: url('<?php echo $background; ?>');">
                            <li class="h1">
                                <a <?php echo $a_attr; ?>href="<?php echo ($url ? $url : '#'); ?>" rel="nofollow">
                                    <?php echo $name; ?>
                                </a>
                            </li>

                            <?php
                            foreach($nav_items_sub as $item_sub) {
                                $name = $item_sub['name'];
                                $url = xcore_sm_generate_link($item_sub['browserUrl']);
                                $links = $item_sub['children'];
                                ?>
                                <li>
                                    <div class="tiles">
                                        <a href="<?php echo ($url ? $url : '#'); ?>" rel="nofollow" class="dropdown-menu-title">
                                            <?php echo $name; ?>
                                        </a>

                                        <?php echo xcore_sm_nav_links($links); ?>
                                    </div>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    <?php } ?>
                </li>
                <?php
            }
        }

        $output .= ob_get_contents();
        ob_end_clean();
    }

    return $output;
}

function xcore_sm_generate_category_navigation_mobile($items) {
    $output = '';

    if($items) {
        ob_start();

        foreach($items as $item_main) {
            $id = $item_main['id'];
            $name = $item_main['name'];
            $url = xcore_sm_generate_link($item_main['browserUrl']);
            $children = $item_main['children'];

            if($name) {
                $li_attr = ($children ? 'class="dropdown dropdown-submenu" data-id="' . $id . '"' : '');
                $a_attr = ($children ? 'class="dropdown-toggle" data-toggle="dropdown" ' : '');
                ?>
                <li <?php echo $li_attr; ?>><a <?php echo $a_attr; ?> href="<?php echo ($url ? $url : '#'); ?>" rel="nofollow"><?php echo $name; ?></a>

                    <?php
                    if($children) {
                        $parent= $item_main;
                        ?>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-menu-back" data-target="<?php echo $id; ?>"><i class="fa fa-angle-left"></i> <?php _e('Alle Kategorien', 'xcore'); ?></a></li>
                            <li><a class="dropdown-menu-title" href="<?php echo ($url ? $url : '#'); ?>" rel="nofllow"><?php echo $name; ?></a></li>

                            <?php foreach($children as $child) { echo xcore_sm_generate_category_navigation_mobile_sub_item($child, $parent); } ?>
                        </ul>
                    <?php
                    }
                    ?>
                </li>
                <?php
            }
        }

        $output .= ob_get_contents();
        ob_end_clean();
    }

    return $output;
}

function xcore_sm_generate_category_navigation_mobile_sub_item($item, $parent) {
    $output = '';

    ob_start();

    if($item) {
        $id = $item['id'];
        $name = $item['name'];
        $url = xcore_sm_generate_link($item['browserUrl']);
        $children = $item['children'];
        $parent_name = $parent['name'];
        $li_attr = ($children ? 'class="dropdown dropdown-submenu" data-id="' . $id . '"' : '');
        $a_attr = ($children ? 'class="dropdown-toggle" data-toggle="dropdown" ' : 'href="' . $url . '"');
        ?>
        <li <?php echo $li_attr; ?>><a <?php echo $a_attr; ?> rel="nofllow"><?php echo $name; ?></a>
            <?php
            if ($children) {
                $parent= $item;
                ?>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-menu-back" data-target="<?php echo $id; ?>"><i class="fa fa-angle-left"></i> <?php echo $parent_name; ?></a></li>
                    <li><a class="dropdown-menu-title" href="<?php echo($url ? $url : '#'); ?>" rel="nofollow"><?php echo $name; ?></a></li>

                    <?php foreach($children as $child) { echo xcore_sm_generate_category_navigation_mobile_sub_item($child, $parent); } ?>
                </ul>
                <?php
            }
            ?>
        </li>
        <?php
    }

    $output .= ob_get_contents();
    ob_end_clean();

    return $output;
}

function xcore_bm_get_themenwelt_menu_items() {
    /**
     * BER: 22
     * MUC: 23
     */
    $location = xcore_sm_current_location();
    $nav_id = 22;
    if($location['short'] == 'muc') $nav_id = 23;

    $menu_items = xcore_build_tree(wp_get_nav_menu_items($nav_id));

    return $menu_items;

    if($menu_items) {
        foreach($menu_items as $k => $v) {
            if(strpos($v->post_title, 'Themenwelt') !== false) {
                return $v->wpse_children;
            }
        }
    }

    return false;
}

function xcore_sm_generate_themenwelt_navigation($items) {
    $output = '';

    if($items) {
        $last_parent = '';
        foreach($items as $item) {
            $output .= xcore_sm_generate_themenwelt_navigation_item($item);
        }
    }

    return $output;
}

function xcore_sm_generate_themenwelt_navigation_item($item, $last_parent) {
    $output = '';

    if($item) {
        $childrens = (isset($item->wpse_children) ? $item->wpse_children : false);

        $output .= '<li class="menu-item menu-item-' . $item->ID . '" data-id="' . $item->ID . '">';
            $output .= '<a href="' . $item->url . '"' . ($childrens ? ' class="dropdown-toggle" data-toggle="dropdown"' : '') . '>';
                $output .= $item->title;
            $output .= '</a>';

            if($childrens) {
                $output .= '<ul class="dropdown-menu">';
                    $output .= '<li><a class="dropdown-menu-back" data-target="' . $item->ID . '"><i class="fa fa-angle-left"></i> ' . __('Alle Kategorien', 'xcore') . '</a></li>';
                    $output .= '<li><a class="dropdown-menu-title" href="' . ($item->url ? $item->url : '#') . '">' . $item->title . '</a></li>';

                    foreach($childrens as $child) {
                        $output .= xcore_sm_generate_themenwelt_navigation_item($child);
                    }
                $output .= '</ul>';
            }

        $output .= '</li>';
    }

    return $output;
}