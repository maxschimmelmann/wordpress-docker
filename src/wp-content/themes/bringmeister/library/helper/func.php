<?php
/*
 * Diverse Hilfsfunktionen
 * 
 * @author		Christian Lang
 * @version		1.0
 * @category	helper
 */
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

/*
 * Formatierte Ausgabe eines Arrays
 */
function xcore_debug($var) {
	echo '<pre>'; print_r($var); echo '</pre>';
}

/*
 * Ist fuer deine Sicherheit...!
 * @src 		http://www.smashingmagazine.com/2010/07/01/10-useful-wordpress-security-tweaks/
 */
remove_action('wp_head', 'wp_generator');

/*
 * Thumbnail Support
 */
add_theme_support( 'post-thumbnails' );
if(false === get_option("medium_crop")) { add_option("medium_crop", "1"); } else { update_option("medium_crop", "1"); }
if(false === get_option("large_crop")) { add_option("large_crop", "1"); } else { update_option("large_crop", "1"); }

/*
 * Funktion für Thumbnails
 */
function xcore_post_thumbnail($post_id, $size = 'thumbnail', $args) {
	$output = '';
	
	if(has_post_thumbnail($post_id)) {
		$output = get_the_post_thumbnail($post_id, $size, $args);
	} else {
		if('1' == get_field('blog_placeholders', 'option')) {
			$sizes = get_image_sizes();
			$output = '<img src="' . esc_url( get_template_directory_uri() ) . '/_/img/placeholder-'.$sizes[$size]["width"].'x'.$sizes[$size]["height"].'.jpg" width="'.$sizes[$size]["width"].'" height="'.$sizes[$size]["height"].'" alt="'.get_the_title($post_id).'"';
			if(isset($args['class']))
				$output .= 'class="'.$args['class'].'"';
			$output .= '/>';
		}
	}
	return $output;
}

/*
 * JPEG Quality 100%
 */
add_filter( 'jpeg_quality', create_function( '', 'return 100;' ) );

/*
 * E-Mail Benachrichtung von Updates deaktvieren
 */
add_filter( 'auto_core_update_send_email', '__return_false' );

/*
 * Entferne style-Tag vom Gallery Shortcode
 */
add_filter( 'use_default_gallery_style', '__return_false' );

/*
 * Entferne <p>-Tag um Bilder
 */
add_filter('the_content', 'xcore_filter_ptags_on_images');
function xcore_filter_ptags_on_images($content){
	return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

/*
 * Span um Widget Counter
 */
add_filter('wp_list_categories', 'xcore_categories_count');
function xcore_categories_count($links) {
	$links = str_replace('</a> (', '</a><span class="count">(', $links);
	$links = str_replace(')', ')</span>', $links);
	return $links;
}

add_filter('get_archives_link', 'xcore_archive_count');
function xcore_archive_count($links) {
	$links = str_replace('</a>&nbsp;(', '</a><span class="count">(', $links);
	$links = str_replace(')', ')</span>', $links);
	return $links;
}

/* 
 * PDF-Filter in der Mediathek
 */
add_filter( 'post_mime_types', 'modify_post_mime_types' );
function modify_post_mime_types( $post_mime_types ) {
	$post_mime_types['application/pdf'] = array( 
		__( 'PDFs', 'xcore' ), 
		__( 'PDFs Verwalten', 'xcore' ), 
		_n_noop( 'PDF <span class="count">(%s)</span>', 
		'PDFs <span class="count">(%s)</span>' ) );
	return $post_mime_types;
}

/*
 * SVG in der Mediathek erlauben
 */
add_filter('upload_mimes', 'xcore_media_mime_types');
function xcore_media_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}

/*
 * Eigene Excerpt Funktion
 */
function endcore_excerpt($limit) {
	$excerpt = explode(' ', get_the_excerpt(), $limit);
	if (count($excerpt)>=$limit) {
		array_pop($excerpt);
		$excerpt = implode(" ",$excerpt).'...';
	} else {
		$excerpt = implode(" ",$excerpt);
	} 
	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	return $excerpt;
}

/* 
 * Browser & OS Body-Classes
 */
add_filter('body_class','endcore_browser_body_class');
function endcore_browser_body_class($classes) {
	global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;
	if($is_lynx) $classes[] = 'lynx';
	elseif($is_gecko) $classes[] = 'gecko';
	elseif($is_opera) $classes[] = 'opera';
	elseif($is_NS4) $classes[] = 'ns4';
	elseif($is_safari) $classes[] = 'safari';
	elseif($is_chrome) $classes[] = 'chrome';
	elseif($is_IE) {
		$classes[] = 'ie';
		if(preg_match('/MSIE ([0-9]+)([a-zA-Z0-9.]+)/', $_SERVER['HTTP_USER_AGENT'], $browser_version))
			$classes[] = 'ie'.$browser_version[1];
	} else $classes[] = 'unknown';
	if($is_iphone) $classes[] = 'iphone';
	if ( stristr( $_SERVER['HTTP_USER_AGENT'],"android") ) {
		 $classes[] = 'android';
	} else if ( stristr( $_SERVER['HTTP_USER_AGENT'],"mac") ) {
		 $classes[] = 'osx';
	} elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"linux") ) {
		 $classes[] = 'linux';
	} elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"windows") ) {
		 $classes[] = 'windows';
	}

	if(xcore_phone_detect()) {
		$classes[] = 'is-phone';
	}

	if(xcore_tablet_detect()) {
		$classes[] = 'is-tablet';
	}

	if(!xcore_phone_detect() && !xcore_tablet_detect()) {
		$classes[] = 'is-desktop';
	}

	// location classes
	$location = xcore_sm_current_location();
	if($location && !is_front_page()) {
		$classes[] = 'location-' . $location['short'];
	}

	return $classes;
}

/*
 * User Admin Body Class
 */
add_filter('admin_body_class','endcore_user_in_admin_body_class');
function endcore_user_in_admin_body_class($classes) {
	$current_user = wp_get_current_user();
	
	if($current_user)
		$classes .= $current_user->user_login;
	
	return $classes;
}

/*
 * Hilfsfunkion für aktuell angelegte Thumbnail Sizes
 */
function get_image_sizes( $size = '' ) {
	global $_wp_additional_image_sizes;

	$sizes = array();
	$get_intermediate_image_sizes = get_intermediate_image_sizes();

	foreach( $get_intermediate_image_sizes as $_size ) {
		if ( in_array( $_size, array( 'thumbnail', 'medium', 'large' ) ) ) {
			$sizes[ $_size ]['width'] = get_option( $_size . '_size_w' );
			$sizes[ $_size ]['height'] = get_option( $_size . '_size_h' );
			$sizes[ $_size ]['crop'] = (bool) get_option( $_size . '_crop' );
		} elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {
			$sizes[ $_size ] = array( 
				'width' => $_wp_additional_image_sizes[ $_size ]['width'],
				'height' => $_wp_additional_image_sizes[ $_size ]['height'],
				'crop' =>  $_wp_additional_image_sizes[ $_size ]['crop']
			);
		}
	}

	if ( $size ) {
		if( isset( $sizes[ $size ] ) ) {
				return $sizes[ $size ];
		} else {
				return false;
		}
	}

	return $sizes;
}

/*
 * Extra Klasse für angelegte Bilder im TinyMCE
 */
add_filter('image_send_to_editor','xcore_linked_images_class',10,8);
function xcore_linked_images_class($html, $id, $caption, $title, $align, $url, $size, $alt = '' ){
	$classes = 'lightbox';
	
	if ( preg_match('/<a.*? class=".*?">/', $html) ) {
		$html = preg_replace('/(<a.*? class=".*?)(".*?>)/', '$1 ' . $classes . '$2', $html);
	} else {
		$html = preg_replace('/(<a.*?)>/', '$1 class="' . $classes . '" >', $html);
	}
	return $html;
}

/*
 * HTML in Kommentaren deaktvieren
 */
add_filter( 'preprocess_comment', 'xcore_rmhtml_comment_post', '', 1 );
add_filter( 'comment_text', 'xcore_rmhtml_comment_display', '', 1 );
add_filter( 'comment_text_rss', 'xcore_rmhtml_comment_display', '', 1 );
add_filter( 'comment_excerpt', 'xcore_rmhtml_comment_display', '', 1 );
remove_filter( 'comment_text', 'make_clickable', 9 );
function xcore_rmhtml_comment_post( $incoming_comment ) {
	$incoming_comment['comment_content'] = htmlspecialchars($incoming_comment['comment_content']);
	$incoming_comment['comment_content'] = str_replace( "'", '&apos;', $incoming_comment['comment_content'] );
	return( $incoming_comment );
}
function xcore_rmhtml_comment_display( $comment_to_display ) {
	$comment_to_display = str_replace( '&apos;', "'", $comment_to_display );
	return $comment_to_display;
}

/*
 * Entferne überflüssiges CSS von WP
 */
add_action( 'widgets_init', 'xcore_remove_recent_comment_style' );
function xcore_remove_recent_comment_style() {
	global $wp_widget_factory;
	remove_action('wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ));
}

/*
 * Entferne überflüssige Tags, wenn WPML aktiv.
 */
if ( is_plugin_active('sitepress-multilingual-cms/sitepress.php') ) {
	if(!is_admin()){
	    remove_action( 'wp_head', array($sitepress, 'meta_generator_tag') );
	}
	add_filter( 'meta_generator_tag', 'theme_generator_tag' );

	function theme_generator_tag() {
		return false;
	}
}

/*
 * AJAX_URL definieren
 */
add_action('wp_head','xcore_ajaxurl');
function xcore_ajaxurl() {
    ?>
    <script type="text/javascript">
        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
    </script>
    <?php
}

/**
 * Single article markup
 */
if(!function_exists('xcore_schema_single_article')) {
	add_action( 'wp_footer', 'xcore_schema_single_article', 133337 );
	function xcore_schema_single_article() {
		if ( is_single() && 'post' == get_post_type() ) {
			global $post;
			$logo = get_field('design_logo', 'option');
			$title = get_the_title();
			$author = esc_attr(get_the_author());
			$date = get_the_date('Y-m-d H:i:s');
			$date_modified = get_the_modified_date('Y-m-d H:i:s');
			$image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large');
			$url = get_permalink();

			echo PHP_EOL . '<script type="application/ld+json">' . PHP_EOL;
			echo '{' . PHP_EOL;
			echo '"@context": "http://schema.org/",' . PHP_EOL;
			echo '"@type": "Article",' . PHP_EOL;
			echo '"name": "' . $title . '",' . PHP_EOL;
			echo '"headline": "' . $title . '",' . PHP_EOL;
			echo '"author": {"@type" : "Person", "name" : "' . $author . '"},' . PHP_EOL;
			echo '"publisher": {"@type" : "Organization", "name" : "' . get_bloginfo('name') . '"' . ($logo ? ',"logo" : {"@type" : "ImageObject", "url" : "' . $logo["url"] . '", "width" : "' . $logo["width"] . 'px", "height" : "' . $logo["height"] . 'px"}' : '') . '},' . PHP_EOL;
			echo '"datePublished": "' . $date . '",' . PHP_EOL;
			echo '"dateModified": "' . $date_modified . '",' . PHP_EOL;
			echo '"mainEntityOfPage": "' . $url . '"' . PHP_EOL;
			echo($image ? ',"image": {"@type" : "ImageObject", "url" : "' . $image[0] . '", "width" : "' . $image[1] . 'px", "height" : "' . $image[2] . 'px"}' . PHP_EOL : '');
			echo '}' . PHP_EOL;
			echo '</script>' . PHP_EOL;
		}
	}
}

/**
 * External Link Function
 */
function xcore_parse_external_url( $url, $internal_class = '', $external_class = '' ) {

	// Abort if parameter URL is empty
	if( empty($url) ) {
		return false;
	}

	// Parse home URL and parameter URL
	$link_url = parse_url( $url );
	$home_url = parse_url( home_url() );


	// Decide on target
	if( $link_url['host'] == $home_url['host'] ) {
		// Is an internal link
		$target = '_self';
		$class = $internal_class;
		$rel = 'follow';

	} else {
		// Is an external link
		$target = '_blank';
		$class = $external_class;
		$rel = 'nofollow';
	}

	// Return array
	$output = array(
		'class'     => $class,
		'target'    => $target,
		'url'       => $url,
		'rel'		=> $rel
	);
	return $output;
}

if ( ! function_exists( 'xcore_phone_detect' ) ) {
	/**
	 * xcore_phone_detect function.
	 *
	 * @return boolean
	 */
	function xcore_phone_detect() {
		$detect = new Mobile_Detect;
		if ($detect->isMobile() && !$detect->isTablet()) {
			return true;
		} else {
			return false;
		}
	}
}

if ( ! function_exists( 'xcore_tablet_detect' ) ) {
	/**
	 * xcore_tablet_detect function.
	 *
	 * @return boolean
	 */
	function xcore_tablet_detect() {
		$detect = new Mobile_Detect;

		if ($detect->isTablet()) {
			return true;
		} else {
			return false;
		}
	}
}

function xcore_attribute_array_html($attributes) {
    $attributes_html = '';

    if($attributes) {
        foreach($attributes as $k => $v) {
            if($v) {
                $attributes_html .= $k . '="' . implode($v, ' ') . '" ';
            }
        }
    }

    return $attributes_html;
}

function xcore_pb_editor_class($count) {
    switch($count) {
        case 1:
            return 'col-sm-12';
            break;

        case 2:
            return 'col-sm-6';
            break;

        case 3:
            return 'col-sm-4';
            break;

        case 4:
            return 'col-sm-3';
            break;

        default:
            return 'col-sm-6';
            break;
    }

    return false;
}

function xcore_pb_render_editor($item, $count, $i, $offset = '', $extended = false, $extended_layout = '') {
    $class = $item['class'];
    $class_col = $item['class_col'];
    $bgimage = $item['bgimage'];
    $bgcolor = $item['bgcolor'];
    $no_tiles = $item['no_tiles'];
    $editor = $item['editor'];

    $attributes = array(
        'class' => array(),
        'style' => array(),
    );

    $attributes_inner = array(
        'class' => array('inner'),
        'style' => array()
    );

    if($class) {
        $attributes['class'][] = $class;
    }

    if($extended && $extended_layout) {
        $extended_layout_arr = explode('_');
        if(is_array($extended_layout_arr)) {
            $class_col = $extended_layout_arr[$i - 1];
        }
    }

    if(!$class_col) {
        $attributes['class'][] = xcore_pb_editor_class($count);
    } else {
        $attributes['class'][] = $class_col;
    }



    if($offset) {
        $attributes['class'][] = 'col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1';
    }

    if($bgimage) {
        $attributes['style'][] = 'background-image: url(' . $bgimage['url'] . ');';
    }

    if($bgcolor) {
        $attributes['style'][] = 'background-color: ' . $bgcolor . ';';

        if($bgcolor == '#0c617a') {
            $attributes['class'][] = 'text-white';
        }
    }

    if($no_tiles != '1') {
        $attributes_inner['class'][] = 'tiles';
    }

    if($extended && $bgimage) {
        $attributes['style'] = array();
        $attributes_inner['class'][] = 'with-bg';
        $attributes_inner['style'][] = 'background-image: url(' . $bgimage['url'] . ');';
    }

    if($extended) {
        $editor = '<div class="caption">' . $editor . '</div>';
    }

    $output = '<div ' . xcore_attribute_array_html($attributes) . '>';
        $output .= '<div ' . xcore_attribute_array_html($attributes_inner) . '>';
            $output .= $editor;
        $output .= '</div>';
    $output .= '</div>';

    return $output;
}

function xcore_calculate_grid_class($num) {
    switch($num) {
        case 2:
            return 'col-sm-6';
            break;

        case 3:
            return 'col-sm-4';
            break;

        case 4:
            return 'col-sm-3';
            break;

        case 6:
            return 'col-sm-4';
            break;

        case 8:
            return 'col-sm-3';
            break;
    }

    return 'col-sm-4';
}

add_filter( 'pb_the_content', 'xcore_img_unauto_p', 30 );
function xcore_img_unauto_p($pee) {
    $pee = preg_replace('/<p>\\s*?(<a .*?><img.*?><\\/a>|<img.*?>)?\\s*<\\/p>/s', '<div class="figure">$1</div>', $pee);
    return $pee;
}

function xcore_build_tree( array &$elements, $parentId = 0 ) {
    $branch = array();
    foreach ( $elements as &$element )
    {
        if ( $element->menu_item_parent == $parentId )
        {
            $children = xcore_build_tree( $elements, $element->ID );
            if ( $children )
                $element->wpse_children = $children;

            $branch[$element->ID] = $element;
            unset( $element );
        }
    }
    return $branch;
}

function xcore_bm_add_image_edit_settings() {
    ob_start();
    wp_print_media_templates();
    $tpl = ob_get_clean();
    if ( ( $idx = strpos( $tpl, 'tmpl-image-details' ) ) !== false
        && ( $before_idx = strpos( $tpl, '<div class="advanced-section">', $idx ) ) !== false ) {
        ob_start();
        ?>
        <div class="my_setting-section">
            <h2><?php _e( 'Bringmeister' ); ?></h2>
            <div class="my_setting">
                <label class="setting my_setting">
                    <span><?php _e( 'Name der Kampagne' ); ?></span>
                    <input type="text" data-setting="bm_campaign_name" value="{{ data.model.my_setting }}" />
                </label>
            </div>
        </div>
        <?php
        $my_section = ob_get_clean();
        $tpl = substr_replace( $tpl, $my_section, $before_idx, 0 );
    }
    echo $tpl;
};

// Hack the output of wp_print_media_templates()
add_action('wp_enqueue_media', $func =
    function() {
        remove_action('admin_footer', 'wp_print_media_templates');
        add_action('admin_footer',  'xcore_bm_add_image_edit_settings');
    }
);

add_action( 'wp_enqueue_media', function () {
    add_action( 'admin_footer', function () {
        ?>
        <script type="text/javascript">
            jQuery(function ($) {
                if (wp && wp.media && wp.media.events) {
                    wp.media.events.on( 'editor:image-edit', function (data) {
                        if(jQuery(data.image).parent('a').length > 0) {
                            var el = jQuery(data.image).parent('a');
                        } else {
                            var el = data.image
                        }
                        data.metadata.bm_campaign_name = data.editor.dom.getAttrib(el, 'data-campaign' );
                    } );
                    wp.media.events.on( 'editor:image-update', function (data) {
                        if(jQuery(data.image).parent('a').length > 0) {
                            var el = jQuery(data.image).parent('a');
                        } else {
                            var el = data.image
                        }
                        data.editor.dom.setAttrib(el, 'data-campaign', data.metadata.bm_campaign_name );
                    } );
                }
            });
        </script>
        <?php
    }, 11 );
} );