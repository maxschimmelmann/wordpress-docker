<?php
/*
 * Verarbeitung der Theme Options
 * 
 * @author		Christian Lang
 * @version		1.0
 * @category	helper
 */
 
/*
 * Optionen - Design - Allgemein
 */
function xcore_get_logo($fallback = true) {
	$logo = get_field('design_logo', 'option');
	$logo2x = get_field('design_logo2x', 'option');
	
	if(!$logo && true == $fallback)
		return get_bloginfo('name');
	
	if($logo2x && wp_is_mobile())
		return '<img src="'.$logo2x['url'].'" width="'.$logo2x['width'].'" height="'.$logo2x['height'].'" alt="'.$logo2x['alt'].'" class="img-responsive" />';
	
	if($logo)
		return '<img src="'.$logo['url'].'" width="'.$logo['width'].'" height="'.$logo['height'].'" alt="'.$logo['alt'].'" class="img-responsive" />';
	
	return get_bloginfo('name');
}
add_action('wp_head','xcore_get_favicon');
function xcore_get_favicon() {
	$favicon_ico = get_field('design_favicon', 'option');
	$favicon_touch = get_field('design_favicon_touch', 'option');
	
	if($favicon_ico)
		echo '<link rel="shortcut icon" href="'.$favicon_ico['url'].'" type="image/x-icon" />';
	
	if($favicon_touch)
		echo '<link rel="apple-touch-icon" href="'.$favicon_touch['url'].'" />';
}
function get_wrapper_id() {	
	$setting = get_field('design_layout', 'option');
	
	if($setting == 'fullwidth')
		return 'wrapper-fluid';
	
	if($setting == 'tiles') 
		return 'wrapper-tiles';
	
	return 'wrapper';
}
function get_section_layout($section) {	
	$setting = get_field('design_'.$section.'_layout', 'option');
		
	if($setting)
		return $setting;
		
	return 'boxed';
}
function get_section_layout_class($section) {	
	$setting = get_field('design_'.$section.'_layout', 'option');
	$wrapper_id = get_wrapper_id();
	
	if('nav' == $section && ($wrapper_id != 'wrapper-fluid' || get_section_layout('header') == 'boxed'))
		return 'wrapped';
	
	if($setting == 'boxed' && $wrapper_id == 'wrapper-fluid')
		return 'wrapped';
}
 
/*
* Optionen - Design - Topbar
*/
function xcore_topbar_structure() {
	$setting = get_field('design_topbar_structure', 'option');
	
	if($setting)
		return $setting;
		
	return 'tl_nr';
}
function xcore_get_topbar() {	
	$setting = get_field('design_topbar_show', 'option');
	
	if($setting)
		return true;
}

/*
 * Optionen - Design - Header
 */
function xcore_header_layout() {	
	$setting = get_field('design_header_layout', 'option');
	
	if ($setting)
		return $setting;
	
	return 'boxed';
}
function xcore_header_structure() {	
	$setting = get_field('design_header_structure', 'option');
	
	if($setting)
		return $setting;
	
	return '12';
}
add_filter('wp_footer','xcore_header_sticky_nav', 99);
function xcore_header_sticky_nav() {
	$setting = get_field('design_nav_sticky', 'option');
	
	if($setting)
		echo '<script>
		jQuery(document).ready(function() {
			jQuery("#navigation").affix({
				offset: {
					top: jQuery("#navigation").offset().top,
				}
			});
			
			jQuery("#navigation").on("affix.bs.affix", function () {
				jQuery("#header").addClass("pb50"); 
			});
			
			jQuery("#navigation").on("affix-top.bs.affix", function () {
				jQuery("#header").removeClass("pb50"); 
			});
			
			if ( jQuery("#navigation").hasClass("affix") ) {
				jQuery("#header").addClass("pb50"); 
			}
		});
		</script>';
}
function xcore_header_nav_searchform() {
	$desktop = get_field('design_nav_searchform' , 'option');
	$mobile = get_field('design_nav_mobile_searchform', 'option');
		
	if(('1' == $desktop && '12' == xcore_header_structure()) || '1' == $mobile)
		return true;
	
	return false;
}

/*
 * Optionen - Design - Teaser
 */
function xcore_teaser_hide() {	
	$setting = get_field('design_teaser_hide', 'option');
	
	if ($setting)
		return $setting;
}

/*
 * Optionen - Blog - Allgemein
 */
function xcore_get_sidebar($section) {	
	$setting = get_field('blog_'.$section.'_sidebar', 'option');
	
	if($setting)
		return $setting;
		
	return 'right';
}
function xcore_get_post_layout($section) {	
	$setting = get_field('blog_'.$section.'_post_layout', 'option');
	
	if($setting)
		return $setting;
		
	return 'small';
}
add_filter( 'excerpt_length', 'xcore_excerpt_length', 999 );
function xcore_excerpt_length( $length ) {
	$setting = get_field('blog_excerpt_length', 'option');
	
	if($setting)
		return $setting;
	
	return 55;
}

/*
 * Optionen - Design - Footer
 */
function xcore_footer_structure() {
	$setting = get_field('design_footer_structure', 'option');
	
	if($setting)
		return $setting;
		
	return 'tl_nr';
}
add_filter('body_class','xcore_footer_sticky');
function xcore_footer_sticky($classes) {
	$setting = get_field('design_footer_sticky', 'option');
	$footer_widgets = get_field('design_footer_widgets', 'option');
	
	if('1' == $setting && '1' != $footer_widgets)
		$classes[] = 'sticky-footer';
	
	return $classes;
}

/*
 * Optionen - Allgemein - Anpassen
 */
add_filter('wp_head','xcore_enqueue_custom_css', 99);
function xcore_enqueue_custom_css() {
	$css = get_field('custom_css', 'option');
	
	if($css != "")
		echo $css;
}
add_filter('wp_head','xcore_enqueue_custom_js_header', 999);
function xcore_enqueue_custom_js_header() {
	$js = get_field('custom_js_header', 'option');
	
	if($js != "")
		echo $js;
}
add_filter('wp_footer','xcore_enqueue_custom_js_footer', 999);
function xcore_enqueue_custom_js_footer() {
	$js = get_field('custom_js_footer', 'option');
	
	if($js != "")
		echo $js;
}

/*
 * Optionen - Allgemein - Social Buttons
 */
function xcore_get_social($section) {
	$setting = get_field('socialbuttons_'.$section.'_show', 'option');
	
	if($setting)
		return $setting;
	
	return false;
}

function xcore_get_social_pos($section) {
	$setting = get_field('socialbuttons_'.$section.'_show_pos', 'option');
	
	if($setting)
		return $setting;
	
	return false;
}