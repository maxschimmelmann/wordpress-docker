<?php
/*
 * Laden der Hilfsfunktionen
 * 
 * @author		Christian Lang
 * @version		1.0
 * @category	_load
 */
 
/*
 * Wichtige Dateien
 */
require_once(ENDCORE_HELPER . '/scripts.php');
require_once(ENDCORE_HELPER . '/option.php');
require_once(ENDCORE_HELPER . '/func.php');

/*
 * Stuff
 */
require_once(ENDCORE_HELPER . '/debug.php');
require_once(ENDCORE_HELPER . '/shortcodes.php');
require_once(ENDCORE_HELPER . '/cf7.php');
require_once(ENDCORE_HELPER . '/comments.php');
require_once(ENDCORE_HELPER . '/pagination.php');
require_once(ENDCORE_HELPER . '/social.php');

require_once(ENDCORE_HELPER . '/bringmeister.php');