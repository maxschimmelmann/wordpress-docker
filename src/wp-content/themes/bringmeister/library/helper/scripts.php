<?php
/*
 * enqueue_scripts
 * 
 * @author		Christian Lang
 * @version		1.0
 * @category	helper
 */
$current_user = wp_get_current_user();

if('1' == get_field('custom_debug', 'option')) {
	if('endcore' == $current_user->user_login)
		add_action('wp_enqueue_scripts', 'xcore_load_theme_scripts');
} else {
	add_action('wp_enqueue_scripts', 'xcore_load_theme_scripts');
}
	
function xcore_load_theme_scripts() {
    global $current_user;

	/* Bootstrap CSS */
	wp_enqueue_style('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');

    /* Font-Awesome */
	if('1' == get_field('scripte_fa', 'option')) {
        wp_enqueue_style('font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
    }

    /* Google Fonts */
    wp_enqueue_style('open-sans', 'https://fonts.googleapis.com/css?family=Open+Sans:400,600');
    wp_enqueue_style('font-hind', 'https://fonts.googleapis.com/css?family=Hind:600');
    
    /* Base CSS */
    //if(is_user_logged_in() && $current_user->ID == 1)
    //    wp_enqueue_style('theme', get_template_directory_uri() . '/style-tmp.css');
    //else
	    wp_enqueue_style('theme', get_template_directory_uri() . '/style.css');
	
	/* Bootstrap JS */
	if('1' == get_field('scripte_bootstrap_js', 'option')) {
        wp_enqueue_script('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery'));
    }

    /* Custom Script */
    wp_enqueue_script('scripts', get_template_directory_uri().'/_/js/scripts.js', array('jquery', 'bootstrap'), '1.0', true);
	
	/* Comment Script */
	if(is_single() && !is_home() && comments_open()) {
        wp_enqueue_script('comment-reply');
    }
	
	/* Lightbox */
	if('1' == get_field('scripte_lightbox', 'option')) {
		wp_enqueue_style('lightbox', get_template_directory_uri() . '/_/css/lightbox.css');
		wp_enqueue_script('lightbox', get_template_directory_uri().'/_/js/lightbox.js', array('jquery', 'bootstrap'), '1.0', true);
		
		wp_localize_script('lightbox', 'lightbox_vars',
            array(
                'lightbox_tPrev' => __('Vorheriges Bild (Linke Pfeiltaste)', 'xcore'),
                'lightbox_tNext' => __('Nächstes Bild (Rechte Pfeiltase)', 'xcore'),
                'lightbox_tCounter' => __('%curr% von %total%', 'xcore'),
		    )
	    );
	}

    /* Deregister Open-Sans */
    wp_deregister_style('open-sans');
}

/*
 * TinyMCE Styles
 */
add_action( 'admin_init', 'xcore_editor_styles');
function xcore_editor_styles() {
	add_editor_style(get_template_directory_uri() . '/_/css/editor.css' );
}

/*
 * Backend CSS
 */
add_action('admin_enqueue_scripts', 'xcore_load_backend_scripts');
function xcore_load_backend_scripts() {
	wp_enqueue_style('xcore-backend', get_template_directory_uri() . '/_/css/backend.css');
	
}